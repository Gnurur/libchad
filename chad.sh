# libchad
#
# a start script for chads
# 
#
# Configuration options:
#
# Global:
# - CHAD_DIR: directory of tha game
# - CHAD_EXE: game executable
# - CHAD_MODE: wine or native
# - CHAD_STATE: the state file (.chad_state by default)
# - CHAD_WRAPPER: wrapper command
# - CHAD_LOG: log file
#
# Wine:
# - CHAD_WINDOWED: run in windowed mode
# - CHAD_DXVK: use DXVK
# - CHAD_VKD3D: use VKD3D
# - CHAD_WINE: Wine binary (default: system wine)
# - WINEPREFIX
# - WINEESYNC
# - WINEFSYNC
# And any other environment variables known by other programs
#
# Native:
# - CHAD_RUNTIME: use the runtime
#
#
# Phases and hooks:
#
# libchad runs in several phases. 
# Before each phase chad_hook_pre_<phase> will be called.
# After each phase chad_hook_post_<phase< will be called.
# This can be used to run custom code in the start script for things like calling winetricks
#
# Phase 1: init
# Phase 2: setup
# Phase 3: run
#
# 
# State file:
#
# libchad uses a state file (.chad_state by default) in order to remember which components were installed.
# This file is just a list of installed components, like dxvk and vkd3d-proton.

function usage {
    echo -e "Usage: $0              Start the game"
    echo -e "       $0 update       Force update all compatibility tools"
    echo -e "       $0 clean        Remove the current wine prefix (a backup will be made)"
    exit 1
}

# Logging

function log {
    echo -e "\033[1m[chad] info: $1\033[0m" | tee -a "$LOGFILE"
}

function error {
    (echo -e "\033[1m\e[1;31m[chad] error: $1\e[0m\033[0m" | tee -a "$LOGFILE") && exit 1
}

function warn {
    echo -e "\033[1m\e[1;33m[chad] warning: $1\e[0m\033[0m" | tee -a "$LOGFILE"
}

# State management

verlte() {
    [  "$1" = "$(echo -e "$1\n$2" | sort -V | head -n1)" ]
}

verlt() {
    [ "$1" = "$2" ] && return 1 || verlte $1 $2
}

function get_state_version {
    # Backwards compatibility with old state files
    [ $(grep "$1" ".chad_state" | grep -o ' ' | wc -l) = "0" ] && echo "0.0.0" && return 0

    grep "$1" ".chad_state" | cut -d' ' -f2
}

function get_state {
    grep -q "$1" "$STATEFILE"
}

function add_state {
    get_state "$1 $2" && return 0 

    # Backwards compatibility with old state files
    get_state "$1" && remove_state "$1"

    echo "$1 $2" >> "$STATEFILE"
}

function remove_state {
    sed -i "/$1/d" "$STATEFILE"
}

# Hook helpers

function fn_exists { 
    declare -F "$1" > /dev/null;
}

function hook {
    fn_exists chad_hook_$1_$2 && chad_hook_$1_$2
}

# Utilities

# Download a release of a project from github
#
# $1 - Repository (e.g. HansKristian-Work/vkd3d-proton)
# $2 - File name
# $3 - Release
# $4 - Part of asset url (e.g. ".tar.gz")
function download_github_release {
    RELEASE="$3"
    FILE="${4:-""}"

    curl -Ls "$(curl -s https://api.github.com/repos/$1/releases/$RELEASE | awk -F '["]' '/"browser_download_url":/ {print $4}' | grep "$FILE" | head -n1)" -o "$2"
    [ ! -f "$2" ] && error "Download failed, check internet connection" || return 0
}

function list_github_tags {
    curl -s https://api.github.com/repos/$1/releases | awk -F '["]' '/"tag_name":/ {print $4}' | sort -V -r
}

# Extracts and removes a tarball
#
# $1 - File name
function extract {
    log "Decompressing $1" && tar -xf "$1"
    log "Removing $1" && rm -f "$1"
}

# Phase 1: Initialisation
#
# Set some environment variables that are always needed

function chad_init_common {
    export STATEFILE="${CHAD_STATE:-.chad_state}"
    export LOGFILE="${CHAD_LOG:-chad.log}"

    echo "Starting log on $(date)" >> "$LOGFILE"

    touch "$STATEFILE"

    export GAMEMODE="$(which gamemoderun 2>> "$LOGFILE")"
}

function chad_init_wine {
    export WINEESYNC="${WINEESYNC:-1}"
    export WINEFSYNC="${WINEFSYNC:-1}"
    export WINEDEBUG="${WINEDEBUG:-'-all'}"
    export STAGING_SHARED_MEMORY="${STAGING_SHARED_MEMORY:-1}"
    export WINE_LARGE_ADDRESS_AWARE="${WINE_LARGE_ADDRESS_AWARE:-1}"
    export CHAD_WINE="${CHAD_WINE:-$(which wine)}"
    export WINEDLLOVERRIDES="${WINEDLLOVERRIDES:-"mscoree=d;mshtml=d"}"
    export S32="$WINEPREFIX/drive_c/windows/system32"

    export WINETRICKS="$(which winetricks 2>> "$LOGFILE")"

    [ ! -x "$WINETRICKS" ] \
        && log "winetricks not found, downloading" \
        && curl -L "https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks" \
        && export WINETRICKS=./winetricks

    [ -z ${WINEPREFIX+x} ] \
        && warn "WINEPREFIX not set, using default prefix!"

    [[ ! -d $WINEPREFIX ]] && log "Updating prefix" && $CHAD_WINE wineboot -u && wineserver -w
}

function chad_init_native {
    :
}

function chad_init {
    hook pre init

    chad_init_common
    chad_init_$CHAD_MODE

    hook post init
}

# Phase 2: Setup
#
# Ensure all components are installed or uninstalled

function chad_setup_component {
    if [[ $1 -eq 1 ]]; then
        (get_state $2 && ([[ $CHAD_UPDATE -ne 1 ]] || ! chad_check_$2_update)) || chad_install_$2
    else
        get_state $2 && chad_remove_$2
    fi
}

function chad_setup_wine {
    chad_setup_component "$CHAD_DXVK" "dxvk"
    chad_setup_component "$CHAD_VKD3D" "vkd3d-proton"
}

function chad_setup_native {
    :
}

function chad_setup {
    hook pre setup

    chad_setup_$CHAD_MODE

    hook post setup
}

function latest_dxvk_version {
    list_github_tags doitsujin/dxvk | head -n1 | cut -c 2-
}

function chad_check_dxvk_update {
    CURRENT=$(get_state_version dxvk)
    LATEST=$(latest_dxvk_version)

    verlt "$CURRENT" "$LATEST" && log "DXVK update available: $CURRENT -> $LATEST"
}

function chad_install_dxvk {
    log "Downloading DXVK"

    download_github_release doitsujin/dxvk dxvk.tar.gz latest tar

    rm -rf ./dxvk >> "$LOGFILE" 2>&1
    extract dxvk.tar.gz
    mv ./dxvk-* ./dxvk

    log "Installing DXVK" \
        && WINEPREFIX="$PWD/game/prefix" ./dxvk/setup_dxvk.sh install >> "$LOGFILE" 2>&1 \
        || error "DXVK installation failed"

    add_state dxvk $(latest_dxvk_version)
}

function chad_remove_dxvk {
    log "Uninstalling DXVK"

    ./dxvk/setup_dxvk.sh uninstall
    rm -rf ./dxvk >> "$LOGFILE" 2>&1

    remove_state dxvk
}

function latest_vkd3d-proton_version {
    list_github_tags HansKristian-Work/vkd3d-proton | head -n1 | cut -c 2-
}

function chad_check_vkd3d-proton_update {
    CURRENT=$(get_state_version vkd3d-proton)
    LATEST=$(latest_vkd3d-proton_version)

    verlt "$CURRENT" "$LATEST" && log "VKD3D-Proton update available: $CURRENT -> $LATEST"
}

function chad_install_vkd3d-proton {
    log "Downloading VKD3D-Proton"

    download_github_release HansKristian-Work/vkd3d-proton vkd3d-proton.tar.zst latest tar

    rm -rf ./vkd3d-proton >> "$LOGFILE" 2>&1
    extract vkd3d-proton.tar.zst
    mv ./vkd3d-proton-* ./vkd3d-proton
    
    log "Installing VKD3D-Proton" \
        && WINEPREFIX="$PWD/game/prefix" ./vkd3d-proton/setup_vkd3d_proton.sh install >> "$LOGFILE" 2>&1 \
        || error "VKD3D-Proton installation failed"

    add_state vkd3d-proton $(latest_vkd3d-proton_version)
}

function chad_remove_vkd3d-proton {
    log "Uninstalling VKD3D-Proton"

    ./vkd3d-proton/setup_vkd3d_proton.sh uninstall
    rm -rf ./vkd3d-proton >> "$LOGFILE" 2>&1

    remove_state vkd3d-proton
}

# Phase 3: Run the game

function chad_run_wine {
    cd "$CHAD_DIR" || error "Directory does not exist: $CHAD_DIR"

    CMD="$CHAD_EXE"

    if [ -z ${CHAD_WINDOWED} ]; then
        CMD="$CHAD_WINE $CMD"
    else
        RESOLUTION=$(xrandr | awk -F '[ , ]' '/current/ {print $9 $10 $11}')
        CMD="$CHAD_WINE explorer /desktop=Game,$RESOLUTION $CMD"
    fi

    [ -x "$GAMEMODE" ] && CMD="gamemoderun $CMD"

    eval "$CHAD_WRAPPER $CMD"
}

function chad_run_native {
    cd "$CHAD_DIR" || error "Directory does not exist: $CHAD_DIR"

    CMD="$CHAD_EXE"

    [ -x "$GAMEMODE" ] && CMD="gamemoderun $CMD"

    eval "$CHAD_WRAPPER $CMD"
}

function chad_run {
    hook pre run
    chad_run_$CHAD_MODE
    hook post run
}

function chad_clean {
    log "Removing prefix"
    [[ -d $WINEPREFIX ]] && mv "$WINEPREFIX" "${WINEPREFIX}_backup"

    log "Removing state file"
    rm "$STATEFILE"
}

# Entry point
function chad {
    chad_init

    case "$1" in
        "update")
            export CHAD_UPDATE=1
            chad_setup
            ;;
        "clean")
            chad_clean
            ;;
        *)
            chad_setup
            chad_run
            ;;
    esac
}

chad "$@"
