#!/bin/bash
# Example start script

cd "$(dirname "$(realpath "$0")")";

# export WINE="game/files/wine/bin"
export WINEPREFIX="$PWD/game/prefix"
export CHAD_MODE="wine"
export CHAD_DIR="game/files/"
export CHAD_EXE="game.exe"
export CHAD_DXVK=1
export CHAD_VKD3D=1

function chad_hook_pre_setup {
    log "Going to setup things"
}

function chad_hook_post_setup {
    log "Installing vcrun2017"
    #winetricks vcrun2017
}

function chad_hook_pre_run {
    log "Disabling compositor"
    # killall picom
}

function chad_hook_post_run {
    log "Starting compositor"
    error "lol fuk u"
    # picom
}

source ${LIBCHAD:-./chad.sh}

